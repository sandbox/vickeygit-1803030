

Login one time mail to all README



CONTENTS OF THIS FILE

----------------------



  * Introduction

  * Installation

  * Configuration

  * Usage



INTRODUCTION

------------

Maintainer: vignesh rajendran (http://drupal.org/user/1740828)





INSTALLATION

------------

1. Copy lot_mail_all folder to modules directory.

2. At admin/build/modules enable the dependencies module "Login one time".

3. After that enable the "Login one time mail to all" module.





CONFIGURATION

-------------

The configuration page for this module is at:

User management > Login one time > Send mail to all (admin/user/login_one_time/send_to_all)



USAGE

-----

Generally whenever the registered user's login credentails are reset, In that case, we can send login one time link to all registered users via mail.

1. When we are migrating existing website from developement to live mode, we can to send one time login link to all users.

2. When we have changed password collation, we can notify by sending one time login link to reset the new password.

3. When we have changed username, password validation restriction, we can notify by sending one time login link to reset the new password.

4. If you have added new fields in user profile to fill, we can notify by sending one time login link to reset the new password.

5. You can use this module, in different cases depends upon your situtation.
